const axios = require('axios');
const fs = require("fs");

const logger = fs.createWriteStream('teste.txt', {
  flags: 'a'
})

// create alphabet
const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// Array alphabet example : [000], [001]
const arrSequence = []

for (var i = 0; i < 26; i++) {
  for (var j = 0; j < 26; j++) {
    for (var k = 0; k < 26; k++) {
      arrSequence.push([i, j, k])
    }
  }
}

// Generate alphabet to array
const alphabetToArray = [...alphabet]

// Mapping array to alphabet = [000] = [aaa]
const mappingArrToAlphabet = arrSequence.map(item => alphabetToArray[item[0]] + alphabetToArray[item[1]] + alphabetToArray[item[2]])

// Insert row on txt
const insertRow = (data) => {
  if (data && data.items && data.items.length > 0) {
    data.items.forEach(item => {
      logger.write(`Airport name: ${item.display} City name: ${item.city.display} \r\n`)
    })
  }
}

// Request data
mappingArrToAlphabet.forEach((item, index) => {
  axios
    .get(`https://www.decolar.com/suggestions?locale=pt-BR&profile=sbox-cp-vh&hint=${item}&fields=city`)
    .then(response => {
      const items = response.data.items
      items.forEach(data => {
        if (data.group === 'AIRPORT') {
          insertRow(data)
        }
      });
    })
    .catch(err => {
      console.log(err.response)
    })
})
